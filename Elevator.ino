#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>

// LCD configuration
LiquidCrystal_I2C lcd(32, 16, 2);

// Keypad configuration
const byte ROWS = 4;
const byte COLS = 3;
char keys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};
byte rowPins[ROWS] = {9, 8, 7, 6};
byte colPins[COLS] = {5, 4, 3};
Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

// Pin configuration
const int buzzerPin = 13;
const int motorPin = 10;
const int redLEDPin = 12;
const int greenLEDPin = 11;

void setup() {
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Elevator");

  pinMode(buzzerPin, OUTPUT);
  pinMode(motorPin, OUTPUT);
  pinMode(redLEDPin, OUTPUT);
  pinMode(greenLEDPin, OUTPUT);
}

void loop() {
  char key = keypad.getKey();

  if (key) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Start flow1");
    int num = key - '0';

    if (num >= 1 && num <= 9) {
      int buzzCount = num * 2;
      int motorDuration = num * 1000;

      // Buzzer
      for (int i = 0; i < buzzCount; i++) {
        digitalWrite(buzzerPin, HIGH);
        delay(2000);
        digitalWrite(buzzerPin, LOW);
        delay(2000);
      }

      // Motor
      digitalWrite(motorPin, HIGH);
      delay(motorDuration);
      digitalWrite(motorPin, LOW);

      // RGB LED
      if (num % 2 == 0) {
        digitalWrite(redLEDPin, LOW);
        digitalWrite(greenLEDPin, HIGH);
      } else {
        digitalWrite(greenLEDPin, LOW);
        digitalWrite(redLEDPin, HIGH);
      }

      // Turn off LEDs after the motor duration
      delay(motorDuration);
      digitalWrite(redLEDPin, LOW);
      digitalWrite(greenLEDPin, LOW);
    }
  }
}
